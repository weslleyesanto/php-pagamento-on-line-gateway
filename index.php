<?php include_once('include\config.php');
                            //conn, find, HAVING, id
$execute_select = get_usuario_doacao($conn, false, FALSE, false); 

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Listagem Usuários Doação</title>
    <?php include_once('include\css.php'); ?>
</head>

<body>

    <div id="wrapper">

     <?php include_once('include\menu.php'); ?>

     <div id="page-wrapper">


        <div class="container-fluid">
            <h1>Listagem Perfil</h1>
            <div id="alert" style="display:none;"> </div>
            
            <div id="">
                <a href="usuario_doacao_form.php?q=s&acao=doar" title="Efetue uma Doação">Efetue uma Doação</a>
                
                <table id="listaUsuarioDoacao" class="tablesorter">
                    <?php  if($execute_select->rowCount() > 0){ ?>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th>E-mail</th>
                            <th>Sigla/Estado/Cidade/Bairro</th>
                            <th>Endereço</th>
                            <th>Telefone</th>
                            <th>Valor Doção</th>
                            <th>Tipo Doação</th>
                            <th>Opção de Doação</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php foreach($execute_select as $row): 
                        //DADOS
                       $id_usuario_doacao = $row["id_usuario_doacao"];
                       $nm_usuario_doacao = utf8_encode($row["nm_usuario_doacao"]);
                       $email_usuario_doacao = $row["email_usuario_doacao"];
                        //ENDEREÇO COMPLETO
                       $sg_estado = $row["sg_estado"];
                       $nm_estado = $row["nm_estado"];
                       $nm_cidade = $row["nm_cidade"];
                       $nm_bairro = utf8_encode($row["nm_bairro"]);

                       $nm_endereco = utf8_encode($row["nm_endereco"]);
                       $nr_endereco = $row["nr_endereco"];
                       $nm_complemento = utf8_encode($row["nm_complemento"]);

                       $nr_telefone = formatar_telefone($row["cd_telefone"]);
                       $vl_doacao = number_format($row["vl_doacao"], 2, ',','.');

                       $nm_tipo_doacao = utf8_encode($row["nm_tipo_doacao"]);
                       $nm_opcao_doacao = utf8_encode($row["nm_opcao_doacao"]);
                       $nm_img_opcao_doacao = $row["nm_img_opcao_doacao"];


                       ?>
                       <tr data-id="<?=$id_usuario_doacao?>" modulo="perfil" page="index">
                        <td><?=$id_usuario_doacao?></td>
                        <td><?=$nm_usuario_doacao?></td>
                        <td><?=$email_usuario_doacao?></td>
                        <td><?=$sg_estado. ' / ' .$nm_estado. ' / ' . $nm_cidade . ' / '.$nm_bairro?></td>
                        <td><?=$nm_endereco . ', '. $nr_endereco. ' - ' .$nm_complemento ?></td>
                        <td class="nr_telefone"><?=$nr_telefone?></td>
                        <td class="vl_doacao"><?=$vl_doacao?></td>
                        <td><?=$nm_tipo_doacao?></td>
                        <td><img src="<?=RELATIVO_ICONE.$nm_img_opcao_doacao?>" alt="<?=$nm_opcao_doacao?>" title="<?=$nm_opcao_doacao?>"/></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php }else{ ?>
        <tr>
            <td colspan="6">Nenhum doador encontrado!</td>
        </tr>
        <?php }?>
    </div>
</div>
<!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<?php include_once('include\js.php'); ?>
<script src="assets/js/usuario_doacao.js"></script>
</body>

</html>
