<?php include_once('include\config.php');

define('URL', 'index.php');

if(isset($_POST)){
					//$conn, $dados_post, $dados_file, $DEBUG
	validacao_usuario_doacao($conn, $_POST, false); 

	$retorno = array();
	$redicionar = "";
	
    //'PEGANDO CAMPOS PASSADOS DO FORMULÁRIO PARA AS VARIÁVEIS'
	$nm_usuario_doacao = limpar(utf8_decode($_POST['nm_usuario_doacao']));
	$email_usuario_doacao = limpar($_POST['email_usuario_doacao']);
	$sg_estado = limpar(utf8_decode(strtoupper($_POST['sg_estado'])));
	$nm_estado = get_estado($sg_estado);
	$nm_cidade = limpar($_POST['nm_cidade']);
	$nm_bairro = limpar(utf8_decode($_POST['nm_bairro']));
	$nm_endereco = limpar(utf8_decode($_POST['nm_endereco']));
	$nr_endereco = limpar($_POST['nr_endereco']);
	$nm_complemento = limpar(utf8_decode($_POST['nm_complemento']));
	//PEGA O CEP
	$cep = limpar($_POST['cep']);
	$cep = str_replace("-", "", $cep);

	//OBTENDO NUMERO DE TELEFONE
	$nr_telefone = limpar($_POST['nr_telefone']);
	//TRATANDO NUMERO DE TELEFONE
	$nr_telefone = str_replace("(", "", $nr_telefone);
		$nr_telefone = str_replace(" ", "", $nr_telefone);
		$nr_telefone = str_replace("-", "", $nr_telefone);
	//GET DDD
		$ddd = substr($nr_telefone, 0, 2);
	//GET TELEFONE
		$nr_telefone = substr($nr_telefone, 2);
	//OBTENDO O VALOR DOADO
		$vl_doacao = limpar($_POST['vl_doacao']);
	//TRATANDO VALOR
		$vl_doacao = str_replace(".", "", $vl_doacao);
		$vl_doacao = str_replace(",", ".", $vl_doacao);

		$id_tipos_doacoes = limpar($_POST['id_tipos_doacoes']);

		$tipos_doacoes = get_tipos_doacao($conn, $id_tipos_doacoes);

		//DEFINI URL DE RETORNO APOS A COMPRA 
		$url_retorno = 'http://kbrtecdev.hospedagemdesites.ws/Weslley/php/pagamento-online/obrigado.php';

	if($tipos_doacoes->rowCount() > 0){ //VERIFICA SE É MAIOR QUE ZERO
		foreach($tipos_doacoes as $tipo_doacao){
			$qtd_tipo_doacao = $tipo_doacao["qtd_tipo_doacao"];
		}
        //'FINAL LOOP FOREACH DE TIPOS DE AÇÕES'
	}

	$id_opcoes_doacoes = limpar($_POST['id_opcoes_doacoes']);

	switch($id_opcoes_doacoes){
	case 1: //PayPal

	/* $items[][0] = quantidade
	 * $items[][1] = Nome
	 * $items[][2] = Descrição
	 * $items[][3] = Preço
	 */
	
	//$items = array(array($qtd_tipo_doacao, DESCRICAO, DESCRICAO, $vl_doacao));
	$items = array(array('1', DESCRICAO, DESCRICAO, $vl_doacao));

	$dados = array('tipos_doacao' => $id_tipos_doacoes);

	$retorno_paypal = pagamento_paypal($dados, $items, true);

	//print_r($retorno_paypal); die();

	$redicionar = $retorno_paypal['redicionar'];
	$reference = $retorno_paypal['token'];

	$retorno['res'] = 'ok';
	$retorno['msg'] = 'Doação gerada com sucesso para PayPal!';

	break; 
    //FINAL CASE 1 = PAYPAL

	case 2: //PagSeguro 

	$reference = md5(date("Y-m-d-h-i-s"));

	$data['currency'] = 'BRL';
	$data['itemId1'] = '0001';
	$data['itemDescription1'] = utf8_decode(DESCRICAO);
	$data['itemAmount1'] = $vl_doacao;
	$data['itemQuantity1'] = '1';
	$data['itemWeight1'] = '1000';
	$data['reference'] = $reference;
	$data['senderName'] = $nm_usuario_doacao;
	$data['senderAreaCode'] = $ddd;
	$data['senderPhone'] = $nr_telefone;
	$data['senderEmail'] = $email_usuario_doacao;
	$data['shippingType'] = '1';
	$data['shippingAddressStreet'] = $nm_endereco;
	$data['shippingAddressNumber'] = $nr_endereco;
	$data['shippingAddressComplement'] = $nm_complemento;
	$data['shippingAddressDistrict'] = $nm_bairro;
	$data['shippingAddressPostalCode'] = $cep;
	$data['shippingAddressCity'] = utf8_decode($nm_estado);
	$data['shippingAddressState'] = $sg_estado;
	$data['shippingAddressCountry'] = 'BRA';

	if($id_tipos_doacoes != '1'){
		$DIA_ATUAL = date('d');
		$MES_DIA_ATUAL = date('m-d');

		//Configurar sua recorrência para cobrança manual, seria na verdade uma pré-aprovação
		$data['preApprovalCharge'] = 'manual';
		//Serve para mostrar ao comprador do que se trata a assinatura, [OBRIGATORIO]
		$data['preApprovalName'] = utf8_decode('Assinatura Doação');
		$data['preApprovalDetails'] = utf8_decode('Todo dia '. $DIA_ATUAL .' será cobrado o valor de ' . $vl_doacao .' para o doação');
		$data['preApprovalAmountPerPayment'] = $vl_doacao;
		
		switch ($id_tipos_doacoes) {
			case '2': //MENSAL
				//determinar qual periodicidade que a cobrança será feita
			$data['preApprovalPeriod'] = 'MONTHLY';
			$data['preApprovalDayOfMonth'] = $DIA_ATUAL;
			$data['preApprovalInitialDate' ] = date('c');
			$data['preApprovalFinalDate'] = date('c', strtotime('+2 years'));
			break;
			
			case '3': //TRIMESTRAL
				//determinar qual periodicidade que a cobrança será feita
			$data['preApprovalPeriod'] = 'TRIMONTHLY';
			$data['preApprovalDayOfMonth'] = $DIA_ATUAL;
			$data['preApprovalInitialDate' ] = date('c');
			$data['preApprovalFinalDate'] = date('c', strtotime('+2 years'));
			break;
			case '4': //ANUAL
				//determinar qual periodicidade que a cobrança será feita
			$data['preApprovalPeriod'] = 'YEARLY';
			$data['preApprovalDayOfYear'] = $MES_DIA_ATUAL;

			$data['preApprovalInitialDate' ] = date('c');
			$data['preApprovalFinalDate'] = date('c', strtotime('+2 years'));
			break;
		}
		
		$data['preApprovalMaxAmountPerPeriod'] = $vl_doacao;
		$data['preApprovalMaxTotalAmount'] = $vl_doacao;

	}

	$redicionar = pagamento_pagseguro($data, true);

	$retorno['res'] = 'ok';
	$retorno['msg'] = 'Doação gerada com sucesso para PagSeguro!';

	break;
	//FINAL CASE 2 = PAGSEGURO

	case 3: //Moip
	
	$reference = md5(date("Y-m-d-h-i-s"));
	
	$url_notificacao = 'http://kbrtecdev.hospedagemdesites.ws/Weslley/php/pagamento-online/retorno.php';
	
	$DIA_ATUAL = date('d');
	$MES_DIA_ATUAL = date('m-d');
	
	switch ($id_tipos_doacoes) {
			case '1': //PARCELA UNICA
			
			$inicio_instrucao = "<InstrucaoUnica TipoValidacao='Transparente'>";
			$final_instrucao = '</InstrucaoUnica>';
			$periodicidade = "";
			$dt_vencimento = "";
			$instrucao = 'Unica';
			break;

			case '2': //MENSAL
			//determinar qual periodicidade que a cobrança será feita
			$inicio_instrucao = '<InstrucaoRecorrente>';
			$final_instrucao = '</InstrucaoRecorrente>';
			$periodicidade = '<Periodicidade Tipo="Mensal">1</Periodicidade>';
			$dt_vencimento = '<DataVencimento>'.date('c').'</DataVencimento>';
			$instrucao = 'Recorrente';
			break;
			
			case '3': //TRIMESTRAL
				//determinar qual periodicidade que a cobrança será feita
			$inicio_instrucao = '<InstrucaoRecorrente>';
			$final_instrucao = '</InstrucaoRecorrente>';
			$periodicidade = '<Periodicidade Tipo="Mensal">3</Periodicidade>';
			$dt_vencimento = '<DataVencimento>'.date('c').'</DataVencimento>';
			$instrucao = 'Recorrente';
			break;
			case '4': //ANUAL
				//determinar qual periodicidade que a cobrança será feita
			$inicio_instrucao = '<InstrucaoRecorrente>';
			$final_instrucao = '</InstrucaoRecorrente>';
			$periodicidade = '<Periodicidade Tipo="Anual">1</Periodicidade>';
			$dt_vencimento = '<DataVencimento>'.date('c').'</DataVencimento>';
			$instrucao = 'Recorrente';
			break;
	
	}

	$dados_moip = array('instrucao' => $instrucao);

	$xml = "
	<EnviarInstrucao>
		".$inicio_instrucao."
			<Razao>".DESCRICAO."</Razao>
			<IdProprio>".$reference."</IdProprio>
			<Valores>
				<Valor moeda='BRL'>".$vl_doacao."</Valor>
			</Valores>
			".$periodicidade."
			<Pagador>
				<Nome>".utf8_encode($nm_usuario_doacao)."</Nome>
				<Email>".$email_usuario_doacao."</Email>
				<IdPagador>".$email_usuario_doacao."</IdPagador>
				<EnderecoCobranca>
					<Logradouro>".utf8_encode($nm_endereco)."</Logradouro>
					<Numero>".$nr_endereco."</Numero>
					<Complemento>".$nm_complemento."</Complemento>
					<Bairro>".utf8_encode($nm_bairro)."</Bairro>
					<Cidade>".$nm_cidade."</Cidade>
					<Estado>".$sg_estado."</Estado>
					<Pais>BRA</Pais>
					<CEP>".$cep."</CEP>
					<TelefoneFixo>".$ddd.$nr_telefone."</TelefoneFixo>
				</EnderecoCobranca>
			</Pagador>
			".$dt_vencimento."
			<URLNotificacao>".$url_notificacao."</URLNotificacao>
			<URLRetorno>".$url_retorno."</URLRetorno>
		".$final_instrucao."
	</EnviarInstrucao>
	";

	$redicionar = pagamento_moip($dados_moip, $xml, true);
	
	if(isset($redicionar['res']) AND $redicionar['res'] === 'error'){
		$retorno['res'] = $redicionar['res'];
		$retorno['msg'] = $redicionar['msg'];
	}else{
		$retorno['res'] = 'ok';
		$retorno['msg'] = 'Doação gerada com sucesso para Moip!';
	}

	break;
	//FINAL CASE 3 = MOIP
	
	default:
	$retorno['res'] = 'error';
	$retorno['msg'] = 'Problema ao identificar opção de doação!';
	break;
}
//FINAL SWITCH

$modulo = "Usuário Doador";
$TABELA = "tb_usuario_doacao";

//'PREPARANDO PARAMETRO PARA INSERIR
$PARAM = "(nm_usuario_doacao, email_usuario_doacao, nm_estado, sg_estado, nm_cidade, nm_bairro, nm_endereco, nr_endereco, nm_complemento, cep, cd_telefone, vl_doacao, id_tipo_doacao, id_opcao_doacao, reference) VALUES ('".$nm_usuario_doacao."','".$email_usuario_doacao ."', '".$nm_estado ."', '".$sg_estado ."', '".$nm_cidade ."','".$nm_bairro."','". $nm_endereco."','". $nr_endereco."','". $nm_complemento."','". $cep."','". $ddd.$nr_telefone."','". $vl_doacao."',". $id_tipos_doacoes.",".$id_opcoes_doacoes.", '".$reference."')";


$insert_row = insert($conn, $TABELA, $PARAM, false, false);

		if (!$insert_row) { //se inseriu retorna TRUE
			//$retorno = array('res' => 'error','msg' => "Problema ao inserir nova doação!");

			$retorno['res'] = 'error';
			$retorno['msg'] = 'Problema ao inserir nova doação!';
		}
		//'FINAL VERIFICA SE INSERIU NA TABELA'
		
		if($retorno['res'] === 'error'){
			echo(json_encode($retorno));	
		}else if($retorno['res'] === 'ok'){

			$retorno['url'] = URL;
			$retorno['url_target'] = $redicionar;

			echo(json_encode($retorno));
		}

		//echo('<pre>'); print_r($retorno); die();

	}else{
		header("location: {$url}");
	}
//'FINAL VERIFICA METODO DE REQUISIÇÃO IGUAL A POST'

	?>