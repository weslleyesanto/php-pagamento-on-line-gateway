<?php
//Arquivo criado para criar funções que seram usadas dentro do sistema

/*################################################################################################'
'########################### INICIO FUNÇÕES PRINCIPAIS DO SISTEMA ################################'
'################################################################################################*/

//################ INICIO FORMATAR NÚMERO TELEFONE

function formatar_telefone($nr_telefone){

	if(strlen($nr_telefone) == 11){ //00111115555 -> 00 11111 5555 -> (00) 11111-5555
		$pattern = '/(\d{2})(\d{5})(\d*)/';	
	}else if(strlen($nr_telefone) == 10){ //0011115555 -> 00 1111 5555 -> (00) 1111-5555
		$pattern = '/(\d{2})(\d{4})(\d*)/';
	}

	return preg_replace($pattern, '($1) $2-$3', $nr_telefone);
	
}
//FINAL FORMATAR NÚMERO TELEFONE #########################'

function limpar($string){

	$string = trim($string);
	$string =str_replace("'","",$string);//aqui retira aspas simples <'>
	$string =str_replace("\\","",$string);//aqui retira barra invertida<\\>
	$string =str_replace("UNION","",$string);//aqui retiro  o comando UNION <UNION>

	$banlist = array('"',"“","”", " insert", " select", " update", " delete", " distinct", " having", " truncate", "replace"," handler", " like", " as ", "or ", "procedure ", " limit", "order by", "group by", " asc", " desc","'","union all", "=", "'", "(", ")", "<", ">", " update", "-shutdown",  "--", "'", "#", "$", "%", "¨", "&", "'or'1'='1'", "--", " insert", " drop", "xp_", "*", " and");

	if(preg_match("/[a-zA-Z0-9]+/", $string)){
		$string = trim(str_replace($banlist,'', $string));
	}

	return $string;

}

function validaEmail($email){
	$er = "/^(([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}){0,1}$/";
	if (preg_match($er, $email)){
		return true;
	} else {
		return false;
	}
}

function select($conn, $TABELA, $PARAM = false, $WHERE, $INNER = false, $GROUP_BY = false, $HAVING = false, $ORDER_BY = false, $DEBUG = false){
		//VERIFICA SE PARAM É FALSE'
	if($PARAM == false){
		$PARAM = "";
	}

		//VERIFICA SE INNER É FALSE'
	if($INNER == false){
		$INNER = "";
	}

	//VERIFICA SE GROUP_BY É FALSE'
	if ($GROUP_BY == false){
		$GROUP_BY = "";
	}
	//VERIFICA SE HAVING É FALSE'
	if ($HAVING == false){
		$HAVING = "";
	}

	//VERIFICA SE GROUP_BY É FALSE'
	if ($ORDER_BY == false){
		$ORDER_BY = "";
	}

		//MONTANDO QUERY'
	$QUERY = "SELECT * " . $PARAM . " FROM " . $TABELA . $INNER . $WHERE . $GROUP_BY.$HAVING.$ORDER_BY;

	if($DEBUG){
		die('###SELECT>>> '.$QUERY);
	}

	return $conn->query($QUERY);
}

function update($conn, $TABELA, $SET, $WHERE, $DEBUG = false){

	//MONTANDO QUERY'
	$QUERY = 'UPDATE ' . $TABELA . $SET .$WHERE;

	if($DEBUG){
		die('###UPDATE>>> '.$QUERY);
	}

	return $conn->query($QUERY);
}

function delete($conn, $TABELA, $WHERE, $DEBUG = false){

		//MONTANDO QUERY'
	$QUERY = 'DELETE FROM '. $TABELA . $WHERE;

	if($DEBUG){
		die('###DELETE>>> '.$QUERY);
	}

	return $conn->query($QUERY);
}

function insert($conn, $TABELA, $PARAM, $LAST_ID = FALSE, $DEBUG = false){

	$QUERY = 'INSERT INTO ' . $TABELA . $PARAM;

	if($DEBUG){
		die('###INSERT>>> '.$QUERY);
	}

	if($LAST_ID){
		$conn->query($QUERY);
		return $conn->lastInsertId($LAST_ID);
	}

	return $conn->query($QUERY);
}

/*################################################################################################'
'########################### FINAL FUNÇÕES PRINCIPAIS DO SISTEMA ################################'
'################################################################################################'

'################################################################################################'
'################################## INICIO FUNÇÕES DO SISTEMA ####################################'
'################################################################################################*/

//'################ INICIO USUARIO DOAÇÃO
function get_usuario_doacao($conn, $find = false, $HAVING = false, $id = false){
	
	$TABELA = "tb_usuario_doacao tud";
	$WHERE =" WHERE  tud.ic_status = '1' AND ttd.ic_status = '1'";
	$INNER = " INNER JOIN tb_tipo_doacao ttd ON tud.id_tipo_doacao = ttd.id_tipo_doacao INNER JOIN tb_opcao_doacao tod ON tud.id_opcao_doacao = tod.id_opcao_doacao "; 
	$GROUP_BY = " GROUP BY tud.id_usuario_doacao";

	if($find){
		$WHERE .= $find;
	}

	if($id){
		$WHERE .= $id;
	}

					//$conn, TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
	return  select($conn, $TABELA, false, $WHERE, $INNER, $GROUP_BY, $HAVING, false, false);

}

function validacao_usuario_doacao($conn, $dados_post, $DEBUG = false){

	if($DEBUG){
		echo('<pre>'); 
		print_r($dados_post); 
		die(); 
	}

	$retorno['res'] = 'ok';

	if($dados_post['nm_usuario_doacao'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha o campo nome!';
	}

	if($dados_post['email_usuario_doacao'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha o campo e-mail!';	
	}

	if($dados_post['sg_estado'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha a sigla do estado!';
	}

	if($dados_post['nm_cidade'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha a cidade!';
	}

	if($dados_post['nm_bairro'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha o bairro!';
	}

	if($dados_post['nm_endereco'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha o endereço!';
	}

	if($dados_post['nr_endereco'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha o número do endereço!';
	}

	if($dados_post['nm_complemento'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha o complemento!';
	}

	if($dados_post['cep'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha o CEP!';
	}

	if($dados_post['nr_telefone'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha o telefone!';
	}

	if($dados_post['vl_doacao'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha o valor da doação!';
	}

	if($dados_post['id_tipos_doacoes'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Selecione um tipo de doação!';
	}

	if($dados_post['id_opcoes_doacoes'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Selecione uma opção de doação!';
	}

	if(isset($retorno['res']) && $retorno['res'] == 'error'){
		echo(json_encode($retorno));
		exit;
	}

}

//'FINAL USUARIO DOAÇÃO #########################'

//'################ INICIO TIPOS DOAÇÃO
function get_tipos_doacao($conn, $get_qtd = false){

	$TABELA = "tb_tipo_doacao ";
	$WHERE = " WHERE ic_status = '1' ";
	$ORDER_BY = " ORDER BY nm_tipo_doacao ASC";

	if($get_qtd){
		$WHERE .= " AND id_tipo_doacao = ".$get_qtd;
	}
							//conn, TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
	return select($conn, $TABELA, false, $WHERE, false, false, false, $ORDER_BY, FALSE);

}
//'FINAL TIPOS DOAÇÃO #########################'

//'################ INICIO OPÇÕES DE DOAÇÃO

function get_opcoes_doacoes($conn){

	$TABELA = "tb_opcao_doacao ";
	$WHERE = " WHERE ic_status = '1' ";
	$ORDER_BY = " ORDER BY nm_opcao_doacao ASC";
							//conn, TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
	return select($conn, $TABELA, false, $WHERE, false, false, false, $ORDER_BY, FALSE);

}
//'FINAL OPÇÕES DE DOAÇÃO #########################'

/*INICIO DEFINED PARA O PAYPAL */
//INICIO SANDBOX
define('USER_SANDBOX', 'comercioexpress-facilitator_api1.kbrtec.com.br');
define('PWD_SANDBOX', 'FPN7FY6XVJUVTB9U');
define('ASSINATURA_SANDBOX', 'Aya616Qm0wEwhJvlkpc3FIR95t6pAOSwEM0eyPnAyJqw-rn3ikyClmT7');
define('PAYPAL_URL_SANDBOX', 'https://www.sandbox.paypal.com/cgi-bin/webscr');
define('API_URL_SANDBOX', 'https://api-3t.sandbox.paypal.com/nvp');
//FINAL SANDBOX

//INICIO PRODUÇÃO
define('USER', 'comercioexpress_api1.kbrtec.com.br');
define('PWD', 'H3BESTJ5N6Q7J4RW');
define('ASSINATURA', 'AFcWxV21C7fd0v3bYYYRCpSSRl31AXesHuF.p6JeSojzlMd1gapbf5SD');
define('PAYPAL_URL', 'https://www.paypal.com/cgi-bin/webscr');
define('API_URL', 'https://api-3t.paypal.com/nvp');
//FINAL PRODUÇÃO
/*FINAL DEFINED PARA O PAYPAL */

//'################ INICIO PAGAMENTO PAYPAL
function pagamento_paypal($dados, $items, $sandbox = false){

	if($sandbox){
		$user = USER_SANDBOX;
		$pswd = PWD_SANDBOX;
		$signature = ASSINATURA_SANDBOX;
		
		//URL da PayPal para redirecionamento, não deve ser modificada
		$paypalURL = PAYPAL_URL_SANDBOX;
		$apiEndpoint = API_URL_SANDBOX;

		//URL de retorno, caso o cliente confirme o pagamento
		$returnURL = 'http://kbrtecdev.hospedagemdesites.ws/Weslley/php/pagamento-online/obrigado.php';
		//URL de cancelamento, caso o cliente desista do pagamento
		$cancelURL = 'http://kbrtecdev.hospedagemdesites.ws/Weslley/php/pagamento-online/obrigado.php';


	}else{
	    //credenciais da API para produção
		$user = USER;
		$pswd = PWD;
		$signature = ASSINATURA;
		
		//URL da PayPal para redirecionamento, não deve ser modificada
		$paypalURL = PAYPAL_URL;
		$apiEndpoint = API_URL;

		//URL de retorno, caso o cliente confirme o pagamento
		$returnURL = 'http://kbrtecdev.hospedagemdesites.ws/Weslley/php/pagamento-online/obrigado.php';
		//URL de cancelamento, caso o cliente desista do pagamento
		$cancelURL = 'http://kbrtecdev.hospedagemdesites.ws/Weslley/php/pagamento-online/cancelada.php';
	}

	//Calculando total do Express Checkout
	$total = 0;

	foreach ($items as $item) {
		$total += $item[0] * $item[3];
	}

	//Campos que serão enviados com a operação SetExpressCheckout
	$nvp = array(
		'PAYMENTREQUEST_0_AMT'           => $total,
		'PAYMENTREQUEST_0_CURRENCYCODE'  => 'BRL',
		'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
    	'RETURNURL'                      => $returnURL, //$returnURL
    	'CANCELURL'                      => $cancelURL, //$cancelURL
    	'METHOD'                         => 'SetExpressCheckout',
    	'VERSION'                        => '108',
    	'PWD'                            => $pswd,
    	'USER'                           => $user,
    	'SIGNATURE'                      => $signature,
    	'NOSHIPPING'                     => 1,
    	'REQCONFIRMSHIPPING'             => 0,
    	'USERACTION'                     => 'COMMIT'	
    	);


	 // Parametros só serão incluídos quando a doação for periódica
	if($dados['tipos_doacao'] != '1'){
		$nvp['L_BILLINGTYPE0'] = 'RecurringPayments';
		$nvp['L_BILLINGAGREEMENTDESCRIPTION0'] = DESCRICAO;
	}


	for ($i = 0, $t = count($items); $i < $t; ++$i) {
		$nvp['L_PAYMENTREQUEST_0_QTY' . $i ] = $items[$i][0];
		$nvp['L_PAYMENTREQUEST_0_NAME' . $i] = $items[$i][1];
		$nvp['L_PAYMENTREQUEST_0_DESC' . $i ] = $items[$i][2];
		$nvp['L_PAYMENTREQUEST_0_AMT' . $i] = $items[$i][3];

		if (isset($items[$i][4])) {
			$nvp['L_PAYMENTREQUEST_0_ITEMCATEGORY' . $i] = $items[$i][4];
		}
	}

	//Executando a operação
	$curl = curl_init();

	curl_setopt($curl, CURLOPT_URL, $apiEndpoint);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($nvp));

	$response = urldecode(curl_exec($curl));

	curl_close($curl);

	$responseNvp = array();

	if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
		foreach ($matches['name'] as $offset => $name) {
			$responseNvp[$name] = $matches['value'][$offset];
		}
	}


	//Se a operação tiver sido bem sucedida, redirecionamos o cliente para o
	//ambiente de pagamento.
	if (isset($responseNvp['ACK']) && $responseNvp['ACK'] == 'Success') {
		$query = array(
			'cmd'    => '_express-checkout',
			'token'  => $responseNvp['TOKEN']
			);

    	//redireciona o cliente para a página de pagamento
		$redicionar = $paypalURL .'/'. http_build_query($query);
	} 

	$retorno['token'] = $responseNvp['TOKEN'];
	$retorno['redicionar'] = $redicionar;

	return $retorno;
}

function GetExpressCheckoutDetails($dados, $sandbox = false){

	echo('<pre>');
	echo('######## dados ########## <br/>');
	print_r($dados);


	if($sandbox){
		$user = USER_SANDBOX;
		$pswd = PWD_SANDBOX;
		$signature = ASSINATURA_SANDBOX;
		
		//URL da PayPal para redirecionamento, não deve ser modificada
		$paypalURL = PAYPAL_URL_SANDBOX;
		$apiEndpoint = API_URL_SANDBOX;

	}else{
	    //credenciais da API para produção
		$user = USER;
		$pswd = PWD;
		$signature = ASSINATURA;
		
		//URL da PayPal para redirecionamento, não deve ser modificada
		$paypalURL = PAYPAL_URL;
		$apiEndpoint = API_URL;
	}

	$curl = curl_init();

	$dados_express = array(
		'USER' => $user, 
		'PWD' => $pswd, 
		'SIGNATURE' => $signature, 
		'METHOD' => 'GetExpressCheckoutDetails',
		'VERSION' => '108', 

		'TOKEN' => $dados['token'], 
		);


	echo('######## dados_express ########## <br/>');
	print_r($dados_express);


	curl_setopt($curl, CURLOPT_URL, $apiEndpoint);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($dados_express));

	$response = urldecode(curl_exec($curl));

	curl_close($curl);

	$responseNvp = array();

	if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
		foreach ($matches['name'] as $offset => $name) {
			$responseNvp[$name] = $matches['value'][$offset];
		}
	}

	return $responseNvp;
}

function criar_perfil_paypal($dados, $sandbox = false){

	/*echo('<pre>');
	echo('######## dados ########## <br/>');
	print_r($dados);*/


	if($sandbox){
		$user = USER_SANDBOX;
		$pswd = PWD_SANDBOX;
		$signature = ASSINATURA_SANDBOX;
		
		//URL da PayPal para redirecionamento, não deve ser modificada
		$paypalURL = PAYPAL_URL_SANDBOX;
		$apiEndpoint = API_URL_SANDBOX;

	}else{
	    //credenciais da API para produção
		$user = USER;
		$pswd = PWD;
		$signature = ASSINATURA;
		
		//URL da PayPal para redirecionamento, não deve ser modificada
		$paypalURL = PAYPAL_URL;
		$apiEndpoint = API_URL;
	}

	switch ($dados['id_tipos_doacoes']) {
			case '2': //MENSAL
				//determinar qual periodicidade que a cobrança será feita
			$periodo = 'Month';
			$frequencia = '1';
			break;
			
			case '3': //TRIMESTRAL
				//determinar qual periodicidade que a cobrança será feita
			$periodo = 'Month';
			$frequencia = '3';
			break;
			case '4': //ANUAL
				//determinar qual periodicidade que a cobrança será feita
			$periodo = 'Year';
			$frequencia = '1';
			break;
		}

		$curl = curl_init();

		$dados_assinatura = array(
			'USER' => $user, 
			'PWD' => $pswd, 
			'SIGNATURE' => $signature, 
			'METHOD' => 'CreateRecurringPaymentsProfile',
			'VERSION' => '108', 
			'TOKEN' => $dados['token'], 
			'PAYERID' => $dados['payerID'], 
			'PROFILESTARTDATE' => date('c'),
			'DESC' => DESCRICAO,
			'BILLINGPERIOD' => $periodo, 
			'BILLINGFREQUENCY' => $frequencia,
			'AMT' => $dados['vl_doacao'],
			'CURRENCYCODE' => 'BRL', 
			'COUNTRYCODE' => 'BR', 
			'MAXFAILEDPAYMENTS' => 3
			//'LOCALECODE' => 'pt_BR', 
			//'TOTALBILLINGCYCLES' => '',
			);


		/*echo('######## dados_assinatura ########## <br/>');
		print_r($dados_assinatura);*/
		

		curl_setopt($curl, CURLOPT_URL, $apiEndpoint);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($dados_assinatura));

		$response = urldecode(curl_exec($curl));

		curl_close($curl);

		$responseNvp = array();

		if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
			foreach ($matches['name'] as $offset => $name) {
				$responseNvp[$name] = $matches['value'][$offset];
			}
		}

		return $responseNvp;
	}

	function do_checkout_details($dados, $sandbox = false){

		if($sandbox){
			$user = USER_SANDBOX;
			$pswd = PWD_SANDBOX;
			$signature = ASSINATURA_SANDBOX;

	//URL da PayPal para redirecionamento, não deve ser modificada
			$paypalURL = PAYPAL_URL_SANDBOX;
			$apiEndpoint = API_URL_SANDBOX;

		}else{
    //credenciais da API para produção
			$user = USER;
			$pswd = PWD;
			$signature = ASSINATURA;

	//URL da PayPal para redirecionamento, não deve ser modificada
			$paypalURL = PAYPAL_URL;
			$apiEndpoint = API_URL;

		}

		$curl = curl_init();

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_URL, $apiEndpoint);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
			'USER' => $user,
			'PWD' => $pswd, 
			'SIGNATURE' => $signature, 
			'METHOD' => 'DoExpressCheckoutPayment',
			'VERSION' => '108',
			'TOKEN' => $dados['token'],
			'PAYERID' => $dados['payerID'],
			'PAYMENTREQUEST_0_AMT' => $dados['vl_doacao'],
			'PAYMENTREQUEST_0_CURRENCYCODE'  => 'BRL',
			'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
			)));


		$response =    curl_exec($curl);

		curl_close($curl);

		$nvp = array();

		if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
			foreach ($matches['name'] as $offset => $name) {
				$nvp[$name] = urldecode($matches['value'][$offset]);
			}
		}

		return $nvp;
	}
//'FINAL PAGAMENTO PAYPAL #########################'

//'################ INICIO PAGAMENTO PAGSEGURO
	function pagamento_pagseguro($data, $sandbox = false){

		$data['email'] = 'comercioexpress@kbrtec.com.br';

		if($sandbox){
			$data['token'] = 'BDBFC16944D54481B19BFC5224912ADB';
			$url_pagseguro = 'https://ws.sandbox.pagseguro.uol.com.br/v2/checkout';
			$url_checkout = 'https://sandbox.pagseguro.uol.com.br/v2/checkout/payment.html?code=';
		}else{
			$data['token'] = 'D828F3C251254EC7BF1383D4164AF316';
			$url_pagseguro = 'https://ws.pagseguro.uol.com.br/v2/checkout';
			$url_checkout = 'https://pagseguro.uol.com.br/v2/checkout/payment.html?code=';
		}


	//para converter o array no formato correto
		$data = http_build_query($data);

	//Inicia o cURL
		$curl = curl_init($url_pagseguro);
	//A URL a ser enviado os dados contem um certificado de segurança, nesse caso vamos ignorá-lo utilizando o parâmetro 
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	//O PagSeguro deverá responder enviando alguma coisa, vamos avisar ao cURL que precisaremos desses dados
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	//Vamos avisar ao cURL que esses dados será enviado via POST
		curl_setopt($curl, CURLOPT_POST, true);
	//PagSeguro só irá aceitar a versão 1.1 do HTTP
		curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
	//iremos informar os dados que o cURL irá transportar.
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
	//Com tudo pronto é hora de executar o cURL e gravar a resposta do PagSeguro 
		$xml = curl_exec($curl);
	//Se os dados de autenticação (e-mail e token) estiverem incorretos o PagSeguro irá exibir o texto: Unauthorized

		if($xml == 'Unauthorized'){
			echo('Opss.. email e/ou Token incorretos!');
		exit;//Mantenha essa linha
	}
	//liberar memoria deveremos fechar o cURL
	curl_close($curl);

	//para converter a resposta em um objeto
	$xml = simplexml_load_string($xml);


	//echo('<pre>'); print_r($xml); die();

	//verificar o PagSeguro enviou algum erro
	if(count($xml->error) > 0){
		
		echo('<pre>');
		print_r($xml->error);
		echo('</pre>');
		
		exit;
	}

	return $url_checkout . $xml->code;
}
//'FINAL PAGAMENTO PAGSEGURO #########################'

//################ INICIO PAGAMENTO MOIP
function pagamento_moip($dados, $xml, $sandbox = false){


	//$xml = new SimpleXMLElement($xml); 

	/*echo('<pre>');
	print_r($xml);
	print_r($dados);
	die('opa...');*/

	if($sandbox){
		$seu_token = 'WDJRGN7IKXR8YRL5HK6INKBWRI8ELVSP';
		$sua_key = 'ZIALWUBVBVZFBXXRAE4NBC5J7PDH4YHI4UPZF0SJ';
		#URL do SandBox - Nosso ambiente de testes
		$url_moip =	'https://desenvolvedor.moip.com.br/sandbox/ws/alpha/EnviarInstrucao/'. $dados['instrucao'];
		#URL para redirecionamento
		$url_checkout = 'https://desenvolvedor.moip.com.br/sandbox/Instrucao.do?token=';

	}else{
		$seu_token = '';
		$sua_key = '';
		#URL DESENVOLVIMENTO
		$url_moip =	'http://www.moip.com.br/ws/alpha/EnviarInstrucao/'.$dados['instrucao'];
		#URL para redirecionamento
		$url_checkout = 'https://www.moip.com.br/Instrucao.do?token=';
	}

	$auth = $seu_token.':'.$sua_key;

	//O HTTP Basic Auth é utilizado para autenticação
	$header[] = "Authorization: Basic " . base64_encode($auth);

	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL,$url_moip);

	#header que diz que queremos autenticar utilizando o HTTP Basic Auth
	curl_setopt($curl, CURLOPT_HTTPHEADER, $header);

	#informa nossas credenciais
	curl_setopt($curl, CURLOPT_USERPWD, $auth);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/4.0");
	curl_setopt($curl, CURLOPT_POST, true);

	//Informa nosso XML de instrução
	curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);

	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	//efetua a requisição e coloca a resposta do servidor do MoIP em $ret
	$ret = curl_exec($curl);
	$err = curl_error($curl);
	curl_close($curl);

	if($ret == ''){
		$retorno = array('res' => 'error' , 'msg' => 'Problema na resposta do servidor Moip, tente novamente mais tarde!');
	}else{

		$xml = new SimpleXMLElement($ret);

		/*print_r($xml);
		die('aqui');*/

		if ($xml->Resposta->Status=='Sucesso'){
			$token = $xml->Resposta->Token;
		}else{
			echo('<pre>'); print_r($ret); die('tah aqui');
		}

		$retorno = $url_checkout . $token;
	}
	return $retorno;
}

//FINAL PAGAMENTO MOIP #########################'

//################ INICIO GET NOME DO ESTADO
function get_estado($sg_estado){

	if(isset($sg_estado) AND $sg_estado != ''){
		switch ($sg_estado) {
			case 'AC': $nm_estado = 'Acre'; break;
			case 'AL': $nm_estado = 'Alagoas'; break;
			case 'AM': $nm_estado = 'Amazonas'; break;
			case 'AP': $nm_estado = 'Amapá'; break;
			case 'BA': $nm_estado = 'Bahia'; break;
			case 'CE': $nm_estado = 'Ceará'; break;
			case 'DF': $nm_estado = 'Distrito Federal'; break;
			case 'ES': $nm_estado = 'Espírito Santo'; break;
			case 'GO': $nm_estado = 'Goiás'; break;
			case 'MA': $nm_estado = 'Maranhão'; break;
			case 'MG': $nm_estado = 'Minas Gerais'; break;
			case 'MS': $nm_estado = 'Mato Grosso do Sul'; break;
			case 'MT': $nm_estado = 'Mato Grosso'; break;
			case 'PA': $nm_estado = 'Pará'; break;
			case 'PB': $nm_estado = 'Paraíba'; break;
			case 'PE': $nm_estado = 'Pernambuco'; break;
			case 'PI': $nm_estado = 'Piauí'; break;
			case 'PR': $nm_estado = 'Paraná'; break;
			case 'RJ': $nm_estado = 'Rio de Janeiro'; break;
			case 'RN': $nm_estado = 'Rio Grande do Norte'; break;
			case 'RO': $nm_estado = 'Rondônia'; break;
			case 'RR': $nm_estado = 'Roraima'; break;
			case 'RS': $nm_estado = 'Rio Grande do Sul'; break;
			case 'SC': $nm_estado = 'Santa Catarina'; break;
			case 'SP': $nm_estado = 'São Paulo '; break;
			case 'SE': $nm_estado = 'Sergipe'; break;
			case 'TO': $nm_estado = 'Tocantins'; break;
			default: $nm_estado = 'Nome do estado não idenficado!'; break;
		}
	}

	return $nm_estado;
}
//FINAL GET NOME DO ESTADO #########################'

/*'################################################################################################'
'#################################### FINAL FUNÇÕES DO SISTEMA ####################################'
'##################################################################################################*/
?>