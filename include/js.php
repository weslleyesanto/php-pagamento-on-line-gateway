<script src="assets/js/jquery.js"></script>

<!-- SCRIPTS GENERICOS DO SISTEMA -->
<script src="assets/js/script.js"></script>

<!-- JMASKED -->
<script src="assets/js/jquery.maskedinput.min.js"></script>

<!-- INICIO TABLE SORTER -->
<script src="assets/js/jquery.tablesorter.js"></script>
<script src="assets/js/jquery.tablesorter.widgets.js"></script>
<!-- FINAL TABLE SORTER -->

<script src="assets/js/jquery-ui.min.js"></script>

<script src="assets/js/jquery.maskMoney.min.js"></script>

<!-- CARREGA ESTADO, CIDADE -->
<script src="assets/js/cidades-estados-1.4-utf8.js"></script>
