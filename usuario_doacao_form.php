<?php 

include_once('include\config.php');

$REDIRECIONA = "index.php";

$tipos_doacoes = get_tipos_doacao($conn, false);
$opcoes_doacoes = get_opcoes_doacoes($conn);

if($_GET["q"] == "s"){
    if($_GET["acao"] == "doar"){
        $title = "Efetuar uma Doação";
        $id_usuario_doacao = "";
        $nm_usuario_doacao = "";
        $email_usuario_doacao = "";
        //ENDEREÇO
        $nm_estado = "";
        $sg_estado = "";
        $nm_cidade = "";
        $nm_bairro = "";
        $nm_endereco = "";
        $nr_endereco = "";
        $nm_complemento = "";
        $cep = "";
        
        $nr_telefone = "";
        $vl_doacao = "";

    }
}else{
    header("location:{$REDIRECIONA}");
}
//'FINAL IF Q IGUAL A S' 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=$title?></title>
    <?php include_once('include\css.php'); ?>
</head>

<body>

    <div id="wrapper">

     <?php include_once('include\menu.php'); ?>

     <div id="page-wrapper">

        <div class="container-fluid">

            <h1><?=$title?></h1>

            <div id="alert" style="display:none;"> </div>

            <form role="form" name="form_post_usuario_doacao" method="post" id="form_post_usuario_doacao">
                <label>Nome</label>
                <input type="text" required="required" placeholder="Nome" id="nm_usuario_doacao" name="nm_usuario_doacao" value="<?=$nm_usuario_doacao?>"  maxlength="100"/>
                <br/><br/>
                <label>E-mail</label>
                <input type="email" autofocus maxlength="100" required="required" placeholder="seu@email.com" id="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" value="<?=$email_usuario_doacao?>" />
                <br/><br/>
                <label>Estado: </label>
                <select id="sg_estado"></select>
                <br/><br/>
                <label>Cidade: </label>
                <select id="nm_cidade">
                    <option value="">Selecione um estado</option>
                </select>
                <br/><br/>
                <label>Bairro: </label>
                <input type="text" required="required" placeholder="Bairro" id="nm_bairro" name="nm_bairro" value="<?=$nm_bairro?>"  maxlength="100"/>
                <br/><br/>
                <label>Endereço: </label>
                <input type="text" required="required" placeholder="Ex: Rua da minha casa" id="nm_endereco" name="nm_endereco" value="<?=$nm_endereco?>"  maxlength="100"/>
                <br/><br/>
                <label>Número: </label>
                <input type="text" required="required" placeholder="Número" id="nr_endereco" name="nr_endereco" value="<?=$nr_endereco?>"  maxlength="10"/>
                <br/><br/>
                <label>Complemento: </label>
                <input type="text" required="required" placeholder="Ex: Casa, Apto" id="nm_complemento" name="nm_complemento" value="<?=$nm_complemento?>"  maxlength="50"/>
                <br/><br/>
                <label>CEP: </label>
                <input type="text" required="required" placeholder="Ex: 188744-000" id="cep" name="cep" value="<?=$cep?>"  maxlength="10"/>
                <br/><br/>
                <label>Telefone: </label>
                <input type="text" required="required" placeholder="Ex: (11)97404-0555" id="nr_telefone" name="nr_telefone" value="<?=$nr_telefone?>"  maxlength="15"/>
                <br/><br/>
                <label>Valor doação: </label>
                <input type="text" required="required" placeholder="Ex: R$ 0,01" id="vl_doacao" name="vl_doacao" value="<?=$vl_doacao?>"  maxlength="10"/>
                <br/><br/>
                <label>Tipo doação:</label>
                <select name="id_tipos_doacoes" id="id_tipos_doacoes">
                    <option value="">Selecione um tipo de doação</option>
                    <?php  
                            if($tipos_doacoes->rowCount() > 0){ //VERIFICA SE É MAIOR QUE ZERO
                                foreach($tipos_doacoes as $tipo_doacao){

                                    $pk_tipo_doacao = $tipo_doacao["id_tipo_doacao"];
                                    $nm_tipo_doacao = utf8_encode($tipo_doacao["nm_tipo_doacao"]);
                                    ?>
                                    <option value="<?=$pk_tipo_doacao?>"><?=$nm_tipo_doacao?></option>
                                    <?php
                                }
                                //'FINAL LOOP FOREACH DE TIPOS DE AÇÕES'
                            }
                            //'FINAL _GET TIPOS DE AÇÕES'
                            ?>
                        </select>               
                        <br/><br/>
                        <label>Opção de Pagamento:</label>
                        <select name="id_opcoes_doacoes" id="id_opcoes_doacoes">
                            <option value="">Selecione uma opção de pagamento</option>
                            <?php  
                            if($opcoes_doacoes->rowCount() > 0){ //VERIFICA SE É MAIOR QUE ZERO
                                foreach($opcoes_doacoes as $opcao_doacao){

                                    $pk_opcao_doacao = $opcao_doacao["id_opcao_doacao"];
                                    $nm_opcao_doacao = utf8_encode($opcao_doacao["nm_opcao_doacao"]);
                                    ?>
                                    <option value="<?=$pk_opcao_doacao?>"><?=$nm_opcao_doacao?></option>
                                    <?php
                                }
                                //'FINAL LOOP FOREACH DE OPÇÕES DE PAGAMENTO'
                            }
                            //'FINAL _GET TIPOS DE OPÇÕES DE PAGAMENTO'
                            ?>
                        </select>  
                        <br/><br/>             
                        <input type="submit" name="Salvar" id="btnSalvar" value="Salvar"/>
                    </form>
                    
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->
        <?php include_once('include\js.php'); ?>
        <script src="assets/js/usuario_doacao.js"></script>
        <script type="text/javascript">
            /*INICIO CARREGA ESTADO CIDADE */
            $(function(){


                new dgCidadesEstados({
                  cidade: document.getElementById('nm_cidade'),
                  estado: document.getElementById('sg_estado')
              })
                /*FINAL CARREGA ESTADO CIDADE */
            });
        </script>

    </body>

    </html>
