var usuario_doacao = (function() {
	var UD = {};

	UD.setDHTML = function() {
		
		$('#form_post_usuario_doacao').submit( function(e){
			e.preventDefault();
		});


		$("#vl_doacao").maskMoney({symbol:"R$",decimal:",",thousands:"."});
		$("#cep").mask('99999-999');

		$("#listaUsuarioDoacao").tablesorter({
			theme: 'blue',
			widgets: ["filter", "zebra"],
			widgetOptions: {
				filter_columnFilters: false,
				filter_saveFilters: true,
				filter_reset: '.reset'
			},
		}); 
		
		$("#nr_telefone").focusout(function(){
			var phone, element;
			element = $(this);
			element.unmask();
			phone = element.val().replace(/\D/g, '');
			if(phone.length > 10) {
				element.mask("(99) 99999-999?9");
			} else {
				element.mask("(99) 9999-9999?9");
			}
		}).trigger('focusout');

		$('#btnSalvar').on('click', function() {


			UD.salvar();
		});

	};

	UD.salvar = function() {

		var nm_usuario_doacao = $('#nm_usuario_doacao').val();
		var email_usuario_doacao = $('#email').val();
		
		var sg_estado = $('#sg_estado').val();
		var nm_cidade = $('#nm_cidade').val();
		var nm_bairro = $('#nm_bairro').val();
		
		var nm_endereco = $('#nm_endereco').val();
		var nr_endereco = $('#nr_endereco').val();
		var nm_complemento = $('#nm_complemento').val();
		var cep = $('#cep').val();
		
		var nr_telefone = $('#nr_telefone').val();
		
		var vl_doacao = $('#vl_doacao').val();

		var id_tipos_doacoes = $('#id_tipos_doacoes').val();
		var id_opcoes_doacoes = $('#id_opcoes_doacoes').val();

		var error = false;

		if (nm_usuario_doacao == '') {
			msg = "Preencha o nome!";
			script.alertError(msg);
			error = error ? error : true;
		}else if(email_usuario_doacao == ''){
			msg = "Preencha o E-mail!";
			script.alertError(msg);
			error = error ? error : true;
		}else if(sg_estado == ''){
			msg = "Preencha a sigla do estado!";
			script.alertError(msg);
			error = error ? error : true;
		}else if(nm_cidade == ''){
			msg = "Preencha a cidade!";
			script.alertError(msg);
			error = error ? error : true;
		}else if(nm_bairro == ''){
			msg = "Preencha o bairro!";
			script.alertError(msg);
			error = error ? error : true;
		}else if(nm_endereco == ''){
			msg = "Preencha o Endereço!";
			script.alertError(msg);
			error = error ? error : true;
		}else if(nr_endereco == ''){
			msg = "Preencha o Número!";
			script.alertError(msg);
			error = error ? error : true;
		}else if(nm_complemento == ''){
			msg = "Preencha o Complemento!";
			script.alertError(msg);
			error = error ? error : true;
		}else if(cep == ''){
			msg = "Preencha o CEP!";
			script.alertError(msg);
			error = error ? error : true;
		}else if(nr_telefone == ''){
			msg = "Preencha o telefone!";
			script.alertError(msg);
			error = error ? error : true;
		}else if(vl_doacao == ''){
			msg = "Preencha o valor da doação!";
			script.alertError(msg);
			error = error ? error : true;
		}else if(id_tipos_doacoes == ''){
			msg = "Selecione o tipo de doação!";
			script.alertError(msg);
			error = error ? error : true;
		}else if(id_opcoes_doacoes == ''){
			msg = "Selecione uma opção de pagamento!";
			script.alertError(msg);
			error = error ? error : true;
		}

		if(!error){

			$('#btnSalvar').attr({'disabled':'disabled'});
			
			var data = {
				nm_usuario_doacao: nm_usuario_doacao,
				email_usuario_doacao: email_usuario_doacao,
				sg_estado: sg_estado,
				nm_cidade: nm_cidade,
				nm_bairro: nm_bairro,
				nm_endereco: nm_endereco,
				nr_endereco: nr_endereco,
				nm_complemento: nm_complemento,
				cep: cep,
				nr_telefone: nr_telefone,
				vl_doacao: vl_doacao,
				id_tipos_doacoes: id_tipos_doacoes,
				id_opcoes_doacoes: id_opcoes_doacoes
			};

			$.post('usuario_doacao_post.php',data, function(ret){
				if(ret.res == 'ok'){
					script.alertSucessoTarget(ret.msg, ret.url, ret.url_target, false); 
				}else{
					script.alertError(ret.msg);
				}
				$('#btnSalvar').removeAttr('disabled');
			}, 'json');

}
//FINAL VERIFICA ERROR DIFERENTE DE TRUE

}

$(function() {
	UD.setDHTML();
});

return UD;
})();